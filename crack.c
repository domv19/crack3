#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"
#include "bintree.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    char *ph = md5(guess, strlen(guess));
    // Compare the two hashes
    if (strcmp(hash, ph) == 1)
    {
        return 1;
    }
    else 
    {
        return 0;
    }
    // Free any malloc'd memory
    
}

// TODO
// Read in the hash file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.
char **read_hashes(char *filename)
{
    FILE *f = fopen(filename, "rb");
    if (!f)
    {
        printf("Can't open dictionary file for reading\n");
        exit(1);
    }
    
    int size = 50;
    char **pwds = (char **)malloc(size * sizeof(char *));
    
    char str[40];
    int i = 0;
    while(fscanf(f, "%s\n", str) != EOF)
    {
        // If array is full, make it bigger
        if (i == size)
        {
            size = size + 10;
            char **newarr = (char **)realloc(pwds, size * sizeof(char *));
            if (newarr != NULL) pwds = newarr;
            else
            {
                printf("Realloc failed\n");
                exit(1);
            }
        }
        char *newstr = (char *)malloc((strlen(str)+1) * sizeof(char));
        strcpy(newstr, str);
        pwds[i] = newstr;
        i++;      
    }
    fclose(f);
    return pwds;
}


// TODO
// Read in the dictionary file and return the tree.
// Each node should contain both the hash and the
// plaintext word.
node *read_dict(char *filename)
{
    FILE *f = fopen(filename, "rb");
    if (!f)
    {
        printf("Can't open dictionary file for reading\n");
        exit(1);
    }
    
    char pw[20];
    struct node *e = (struct node *)malloc(sizeof(struct node));
    while (scanf("%s", pw) != EOF)
    {
        // Hash it
        char *ph = md5(pw, strlen(pw));
        
        // Put hash and plain into entry
        //struct node *e = (struct node *)malloc(sizeof(struct node));
        strcpy(e->hash, ph);
        strcpy(e->plain, pw);
        
        // Extract first 4 characters
        char idx[5];  // Holds the 1st four digits of the hash
        strncpy(idx, ph, 4);    
        
        // Convert into int
        long i = strtol(idx, NULL, 16);
               
        // Store entry at that location in file
        int file_location = i * sizeof(struct node);
        // Seek to the file location
        fseek(f, file_location, SEEK_SET);
        // Write to disk
        fwrite(e, sizeof(struct node), 1, f);
        
        // Go next one
    }
    fclose(f);
    return e;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the hash file into an array of strings
    char **hashes = read_hashes("hashes.txt");

    // TODO: Read the dictionary file into a binary tree
    node *dict = read_dict("rockyou.txt");

    // TODO
    // For each hash, search for it in the binary tree.
    // If you find it, get the corresponding plaintext dictionary
    // entry. Print both the hash and word out.
    // Need only one loop. (Yay!)
    for (int i = 0; i != EOF; i++)
    {
        if (strcmp(hashes[i], dict->hash)== 1)
        {
            printf("%s  %s", dict->hash, dict->plain);
        }
        else {return 0;}
    }
}
